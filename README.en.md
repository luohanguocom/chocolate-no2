# 巧克力二号

#### Description
用C#开发的文本分析和处理的Windows应用窗体程序，小巧轻便。在可视化界面下，支持一键剔除词语，一键替换词语，一键统计词频或统计所有词频，一键生成词云或者标记重点句子。基于JieBa.Net和WordCloudSharp等库开发。后续会继续更新。可用于考研或学习需要的文本分析处理方面。开源方便大家学习交流，希望大家也可以多多剔除建议。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
