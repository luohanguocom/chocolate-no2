﻿namespace 文本分析程序demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InputRichBox = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StopWordCheckBox = new System.Windows.Forms.CheckBox();
            this.FuhaoCheckBox = new System.Windows.Forms.CheckBox();
            this.CountNums = new System.Windows.Forms.TextBox();
            this.DelWordText = new System.Windows.Forms.TextBox();
            this.CountAllWord = new System.Windows.Forms.Button();
            this.CountWords = new System.Windows.Forms.Button();
            this.DelWordBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.ClearTextBox = new System.Windows.Forms.Button();
            this.OutputToInputBtn = new System.Windows.Forms.Button();
            this.CopyBtn = new System.Windows.Forms.Button();
            this.TimetBox = new System.Windows.Forms.RichTextBox();
            this.OutPutRichBox = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ReplaceBtn = new System.Windows.Forms.Button();
            this.SignImportantSentence = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CloudBtn = new System.Windows.Forms.Button();
            this.NewWordText = new System.Windows.Forms.TextBox();
            this.OldWordText = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.关于我们ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label6 = new System.Windows.Forms.Label();
            this.WordsWeightcheckBox = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // InputRichBox
            // 
            this.InputRichBox.ContextMenuStrip = this.contextMenuStrip1;
            this.InputRichBox.Location = new System.Drawing.Point(37, 24);
            this.InputRichBox.Name = "InputRichBox";
            this.InputRichBox.Size = new System.Drawing.Size(665, 286);
            this.InputRichBox.TabIndex = 0;
            this.InputRichBox.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.InputRichBox);
            this.groupBox1.Location = new System.Drawing.Point(366, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(725, 334);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "请输入需要分析的文本";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.StopWordCheckBox);
            this.groupBox2.Controls.Add(this.FuhaoCheckBox);
            this.groupBox2.Controls.Add(this.CountNums);
            this.groupBox2.Controls.Add(this.DelWordText);
            this.groupBox2.Controls.Add(this.CountAllWord);
            this.groupBox2.Controls.Add(this.CountWords);
            this.groupBox2.Controls.Add(this.DelWordBtn);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(319, 363);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "词语处理";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(36, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "?";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "个";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 267);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "显示的词数";
            // 
            // StopWordCheckBox
            // 
            this.StopWordCheckBox.AutoSize = true;
            this.StopWordCheckBox.Location = new System.Drawing.Point(156, 236);
            this.StopWordCheckBox.Name = "StopWordCheckBox";
            this.StopWordCheckBox.Size = new System.Drawing.Size(104, 19);
            this.StopWordCheckBox.TabIndex = 2;
            this.StopWordCheckBox.Text = "去除停用词";
            this.StopWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // FuhaoCheckBox
            // 
            this.FuhaoCheckBox.AutoSize = true;
            this.FuhaoCheckBox.Location = new System.Drawing.Point(61, 236);
            this.FuhaoCheckBox.Name = "FuhaoCheckBox";
            this.FuhaoCheckBox.Size = new System.Drawing.Size(89, 19);
            this.FuhaoCheckBox.TabIndex = 2;
            this.FuhaoCheckBox.Text = "去除符号";
            this.FuhaoCheckBox.UseVisualStyleBackColor = true;
            // 
            // CountNums
            // 
            this.CountNums.Location = new System.Drawing.Point(146, 264);
            this.CountNums.Name = "CountNums";
            this.CountNums.Size = new System.Drawing.Size(46, 25);
            this.CountNums.TabIndex = 1;
            // 
            // DelWordText
            // 
            this.DelWordText.Location = new System.Drawing.Point(61, 41);
            this.DelWordText.Name = "DelWordText";
            this.DelWordText.Size = new System.Drawing.Size(201, 25);
            this.DelWordText.TabIndex = 1;
            // 
            // CountAllWord
            // 
            this.CountAllWord.Location = new System.Drawing.Point(61, 295);
            this.CountAllWord.Name = "CountAllWord";
            this.CountAllWord.Size = new System.Drawing.Size(201, 44);
            this.CountAllWord.TabIndex = 0;
            this.CountAllWord.Text = "统计全部词频";
            this.CountAllWord.UseVisualStyleBackColor = true;
            this.CountAllWord.Click += new System.EventHandler(this.CountAllWord_Click);
            // 
            // CountWords
            // 
            this.CountWords.Location = new System.Drawing.Point(61, 132);
            this.CountWords.Name = "CountWords";
            this.CountWords.Size = new System.Drawing.Size(201, 44);
            this.CountWords.TabIndex = 0;
            this.CountWords.Text = "统计词频";
            this.CountWords.UseVisualStyleBackColor = true;
            this.CountWords.Click += new System.EventHandler(this.CountWords_Click);
            // 
            // DelWordBtn
            // 
            this.DelWordBtn.Location = new System.Drawing.Point(61, 82);
            this.DelWordBtn.Name = "DelWordBtn";
            this.DelWordBtn.Size = new System.Drawing.Size(201, 44);
            this.DelWordBtn.TabIndex = 0;
            this.DelWordBtn.Text = "剔除词语";
            this.DelWordBtn.UseVisualStyleBackColor = true;
            this.DelWordBtn.Click += new System.EventHandler(this.DelWordBtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SaveBtn);
            this.groupBox3.Controls.Add(this.ClearTextBox);
            this.groupBox3.Controls.Add(this.OutputToInputBtn);
            this.groupBox3.Controls.Add(this.CopyBtn);
            this.groupBox3.Controls.Add(this.TimetBox);
            this.groupBox3.Controls.Add(this.OutPutRichBox);
            this.groupBox3.Location = new System.Drawing.Point(366, 381);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(725, 292);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "输出";
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(627, 250);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 36);
            this.SaveBtn.TabIndex = 1;
            this.SaveBtn.Text = "保存";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // ClearTextBox
            // 
            this.ClearTextBox.Location = new System.Drawing.Point(461, 250);
            this.ClearTextBox.Name = "ClearTextBox";
            this.ClearTextBox.Size = new System.Drawing.Size(79, 36);
            this.ClearTextBox.TabIndex = 1;
            this.ClearTextBox.Text = "清空";
            this.ClearTextBox.UseVisualStyleBackColor = true;
            this.ClearTextBox.Click += new System.EventHandler(this.ClearTextBox_Click);
            // 
            // OutputToInputBtn
            // 
            this.OutputToInputBtn.Location = new System.Drawing.Point(346, 250);
            this.OutputToInputBtn.Name = "OutputToInputBtn";
            this.OutputToInputBtn.Size = new System.Drawing.Size(109, 36);
            this.OutputToInputBtn.TabIndex = 1;
            this.OutputToInputBtn.Text = "替换输入框";
            this.OutputToInputBtn.UseVisualStyleBackColor = true;
            this.OutputToInputBtn.Click += new System.EventHandler(this.OutputToInputBtn_Click);
            // 
            // CopyBtn
            // 
            this.CopyBtn.Location = new System.Drawing.Point(546, 250);
            this.CopyBtn.Name = "CopyBtn";
            this.CopyBtn.Size = new System.Drawing.Size(75, 36);
            this.CopyBtn.TabIndex = 1;
            this.CopyBtn.Text = "复制";
            this.CopyBtn.UseVisualStyleBackColor = true;
            this.CopyBtn.Click += new System.EventHandler(this.CopyBtn_Click);
            // 
            // TimetBox
            // 
            this.TimetBox.Enabled = false;
            this.TimetBox.Location = new System.Drawing.Point(37, 250);
            this.TimetBox.Name = "TimetBox";
            this.TimetBox.Size = new System.Drawing.Size(249, 36);
            this.TimetBox.TabIndex = 0;
            this.TimetBox.Text = "";
            // 
            // OutPutRichBox
            // 
            this.OutPutRichBox.Location = new System.Drawing.Point(37, 24);
            this.OutPutRichBox.Name = "OutPutRichBox";
            this.OutPutRichBox.ReadOnly = true;
            this.OutPutRichBox.Size = new System.Drawing.Size(665, 220);
            this.OutPutRichBox.TabIndex = 0;
            this.OutPutRichBox.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.ReplaceBtn);
            this.groupBox4.Controls.Add(this.SignImportantSentence);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.WordsWeightcheckBox);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.CloudBtn);
            this.groupBox4.Controls.Add(this.NewWordText);
            this.groupBox4.Controls.Add(this.OldWordText);
            this.groupBox4.Location = new System.Drawing.Point(12, 381);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(319, 292);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "其他功能";
            // 
            // ReplaceBtn
            // 
            this.ReplaceBtn.Location = new System.Drawing.Point(61, 230);
            this.ReplaceBtn.Name = "ReplaceBtn";
            this.ReplaceBtn.Size = new System.Drawing.Size(201, 44);
            this.ReplaceBtn.TabIndex = 0;
            this.ReplaceBtn.Text = "替换词语";
            this.ReplaceBtn.UseVisualStyleBackColor = true;
            this.ReplaceBtn.Click += new System.EventHandler(this.ReplaceBtn_Click);
            // 
            // SignImportantSentence
            // 
            this.SignImportantSentence.Location = new System.Drawing.Point(61, 110);
            this.SignImportantSentence.Name = "SignImportantSentence";
            this.SignImportantSentence.Size = new System.Drawing.Size(201, 44);
            this.SignImportantSentence.TabIndex = 0;
            this.SignImportantSentence.Text = "标注重点句子";
            this.SignImportantSentence.UseVisualStyleBackColor = true;
            this.SignImportantSentence.Click += new System.EventHandler(this.SignImportantSentence_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(153, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "替换词";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "原词";
            // 
            // CloudBtn
            // 
            this.CloudBtn.Location = new System.Drawing.Point(61, 60);
            this.CloudBtn.Name = "CloudBtn";
            this.CloudBtn.Size = new System.Drawing.Size(201, 44);
            this.CloudBtn.TabIndex = 0;
            this.CloudBtn.Text = "生成词云";
            this.CloudBtn.UseVisualStyleBackColor = true;
            this.CloudBtn.Click += new System.EventHandler(this.CloudBtn_Click);
            // 
            // NewWordText
            // 
            this.NewWordText.Location = new System.Drawing.Point(211, 199);
            this.NewWordText.Name = "NewWordText";
            this.NewWordText.Size = new System.Drawing.Size(71, 25);
            this.NewWordText.TabIndex = 1;
            // 
            // OldWordText
            // 
            this.OldWordText.Location = new System.Drawing.Point(76, 199);
            this.OldWordText.Name = "OldWordText";
            this.OldWordText.Size = new System.Drawing.Size(71, 25);
            this.OldWordText.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于我们ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(1103, 28);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 关于我们ToolStripMenuItem
            // 
            this.关于我们ToolStripMenuItem.Name = "关于我们ToolStripMenuItem";
            this.关于我们ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.关于我们ToolStripMenuItem.Text = "关于我们";
            this.关于我们ToolStripMenuItem.Click += new System.EventHandler(this.关于我们ToolStripMenuItem_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(33, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "?";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // WordsWeightcheckBox
            // 
            this.WordsWeightcheckBox.AutoSize = true;
            this.WordsWeightcheckBox.Location = new System.Drawing.Point(93, 35);
            this.WordsWeightcheckBox.Name = "WordsWeightcheckBox";
            this.WordsWeightcheckBox.Size = new System.Drawing.Size(143, 19);
            this.WordsWeightcheckBox.TabIndex = 2;
            this.WordsWeightcheckBox.Text = "同时生成词频txt";
            this.WordsWeightcheckBox.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Copy,
            this.Paste});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(109, 52);
            // 
            // Copy
            // 
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(210, 24);
            this.Copy.Text = "复制";
            this.Copy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // Paste
            // 
            this.Paste.Name = "Paste";
            this.Paste.Size = new System.Drawing.Size(210, 24);
            this.Paste.Text = "黏贴";
            this.Paste.Click += new System.EventHandler(this.Paste_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 685);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "巧克力二号 demo v1.0  by cyx";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox InputRichBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox DelWordText;
        private System.Windows.Forms.Button CountAllWord;
        private System.Windows.Forms.Button CountWords;
        private System.Windows.Forms.Button DelWordBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox OutPutRichBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 关于我们ToolStripMenuItem;
        private System.Windows.Forms.CheckBox StopWordCheckBox;
        private System.Windows.Forms.CheckBox FuhaoCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CountNums;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button CopyBtn;
        private System.Windows.Forms.Button OutputToInputBtn;
        private System.Windows.Forms.Button ReplaceBtn;
        private System.Windows.Forms.Button SignImportantSentence;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CloudBtn;
        private System.Windows.Forms.TextBox NewWordText;
        private System.Windows.Forms.TextBox OldWordText;
        private System.Windows.Forms.Button ClearTextBox;
        private System.Windows.Forms.RichTextBox TimetBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox WordsWeightcheckBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Copy;
        private System.Windows.Forms.ToolStripMenuItem Paste;
    }
}

